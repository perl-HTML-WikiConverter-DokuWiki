#!/bin/bash

# I developed each fix or feature in a separate topic branch on top of
# upstream 0.53.
#
# This script quickly merges all the topic branch I want into 
# a new branch for testing.
# 
# It shouldn't be considered safe to pull from this testing branch 
# because whenever I change a patch, it becomes a "new" branch not
# inheriting from the old variant:
# 
# if I change a patch, I reset the testing branch, edit the patch,
# then re-create the testing branch from here with:
#
#     git checkout -b TEST
#     ./git-merge-for-testing.sh

# (Hopefully, git-rerere helps me not to resolve conflicts too often.)
# 
# If merging breaks, fix the conflicts, and simply re-run this script.
# It should continue merging (already completed merges will result in
# a trivial fast-forward, i.e., skipped.)

merge() {
    for b in "$@"; do
	if ! git merge -v --log --rerere-autoupdate "$b"; then
	    if git rerere status; then
		git commit
	    else
		echo $"Fix the conflicts manually, and proceed by re-reunning this script!"
		exit 1
	    fi
	fi
    done
}

set -ex

merge \
    syntax/code \
    syntax/links-always-explicit \
    syntax/complex-tables-boring \
    pretty/strip_aname \
    pretty/strip_fonts_on_blank \
    pretty/typography_degrees \
    dirtyhack/strip_underline_blank \
    dirtyhack/join-adjacent-fonts \
    pretty/links-explicit-only-necessary \
    dirtyhack/bodylevel-tables-boring

# Finally, also inherit formally from my old version to allow git-pull
# for those who have got my old version.
git merge -v -s ours 0.54_imz0 \
    -m 'Formal merge to indicate that the new version supercedes the previous release.'