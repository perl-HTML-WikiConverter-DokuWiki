package HTML::WikiConverter::DokuWiki;

use warnings;
use strict;

use base 'HTML::WikiConverter';

our $VERSION = '0.55_01';

=head1 NAME

HTML::WikiConverter::DokuWiki - Convert HTML to DokuWiki markup

=head1 SYNOPSIS

  use HTML::WikiConverter;
  my $wc = new HTML::WikiConverter( dialect => 'DokuWiki' );
  print $wc->html2wiki( $html );

=head1 DESCRIPTION

This module contains rules for converting HTML into DokuWiki
markup. See L<HTML::WikiConverter> for additional usage details.

=head1 ATTRIBUTES

In addition to the regular set of attributes recognized by the
L<HTML::WikiConverter> constructor, this dialect also accepts the
following attributes:

=head2 camel_case

Boolean indicating whether CamelCase links are enabled in the target DokuWiki
instance. Enabling CamelCase links will turn HTML like this

  <p><a href="/wiki:camelcase">CamelCase</a> links are fun.</p>

into this DokuWiki markup:

  CamelCase links are fun.

Disabling CamelCase links (the default) would convert that HTML into

  [[CamelCase]] links are fun.

=cut

sub attributes { {
  camel_case => { default => 0 },
  typography => { default => 1 },
  strip_blank_underline => { default => 0 },
  join_adjacent_fonts => { default => 1 },
  boring_tables_bodylevel => { default => 0 }
} }

# TODO careful treatment of wiki/nowiki:
# 1) escape text resembling wiki format (better to do this
# when processing each node);
# 2) an option to pass some selected untranslatable HTML tags
# through by enclosing them in NOWIKI or HTML or PHP (filtering is
# essential here, otherwise we'd get a mess of useless tags);
# 3) pass the text to postrpocessing so that it can distiguish between
# pieces of wiki-formatted text and nowiki-text.
sub rules {
  my %rules = (
    b => { start => '**', end => '**' },
    strong => { alias => 'b' },
    i => { start => '//', end => '//' },
    em => { alias => 'i' },
    u => { start => '__', end => '__' },

    tt => { start => "''", end => "''" }, # double single-quotes
    code => { alias => 'tt' },
    a => { replace => \&_link },
    img => { replace => \&_image },

    pre => { line_format => 'blocks', line_prefix => '  ', block => 1 },
    blockquote => { start => "\n", line_prefix => '>', block => 1, line_format => 'multi', trim => 'leading' },

    p => { block => 1, trim => 'both', line_format => 'multi' },
    br => { replace => "\\\\ " },
    hr => { replace => "\n----\n" },

    sup => { preserve => 1 },
    sub => { preserve => 1 },
    del => { preserve => 1 },

    ul => { line_format => 'multi', block => 1, line_prefix => '  ' },
    ol => { alias => 'ul' },
    li => { line_format => 'multi', start => \&_li_start, trim => 'leading' },

    table => { block => 1 },
    tr => { start => "\n", line_format => 'single' },
    td => { start => \&_td_start, end => \&_td_end, trim => 'both' },
    th => { alias => 'td' },

    boringtd => { block => 1, trim => 'both', line_format => 'blocks' },
    boringth => { alias => 'boringtd' },
  );

  for( 1..5 ) {
    my $str = ( '=' ) x (7 - $_ );
    $rules{"h$_"} = { start => "$str ", end => " $str", block => 1, trim => 'both', line_format => 'single' };
  }
  $rules{h6} = { start => '== ', end => ' ==', block => 1, trim => 'both', line_format => 'single' };

  return \%rules;
}

# TODO: postprocessing should differentiate between wiki-formatted text and nowiki-text.
sub postprocess_output {
  my( $self, $outref ) = @_;
  $$outref =~ s~^>+\s+~~gm; # rm empty blockquote prefixes
  $$outref =~ s~^(>+)~$1 ~gm; # add space after prefix for clarity

  $self->_reduce_adjacent_formatting($outref) if $self->join_adjacent_fonts; # hack for more clarity

  $self->_render_degrees($outref) if $self->typography; # TODO: unless in nowiki-text.

  $self->_simplify_unnecessary_explicit_links($outref);
}

sub _simplify_unnecessary_explicit_links {
  my( $self, $outref ) = @_;

  $$outref =~ s~(^|\s)\[\[([^\[\]\|]*)\|\2\]\](\s|$)~$1$2$3~gm; # when surrounded
				# by whitespace
}

##############################################################
# A hack to reduce adjacent matching (hence superfluous) text
# formatting markup (like STRONG, EM, U, DEL, CODE) into a
# single occurence of the markup.
# This way the text gets much more readable.
#
# For some kinds of formatting, we'd also like to reduce adjacent 
# markup separated by whitespace (but not newlines -- hence [:blank:]).
# But not for U, DEL, CODE -- because this can produce visual and
# semantic differences.
#
# This is a hack! Don't trust it to never mess up some structure.
# That's because at this stage of postprocessing, we don't know the
# structure (remember, it's impossible to respect structure in
# regular-expression-based formal languages).
# 
# A demonstration that it's a hack is the following "side-effect":
# we also strip stand-alone formatting around blanks or empty text.
# Well, that's not so evil, because this side-effect also leads to
# more readable text by reducing superfluous markup (most of the
# time).
sub _reduce_adjacent_formatting {
  my( $self, $outref ) = @_;

  $$outref =~ s~\*\*([[:blank:]]*)\*\*~$1~gm; # STRONG
  $$outref =~ s~//([[:blank:]]*)//~$1~gm; # EM
  $$outref =~ s~____~~gm; # U
  $$outref =~ s~''''~~gm; # CODE
  $$outref =~ s~</del><del>~~gm; # DEL
}

# Render poor man's degrees nicely (with Unicode):
use charnames ':full', ':short';
sub _render_degrees {
  my( $self, $outref ) = @_;

  $$outref =~ s~<sup>(\N{latin:o}|\N{cyrillic:o})</sup>F~\N{DEGREE SIGN}F~gm;
  $$outref =~ s~<sup>(\N{latin:o}|\N{cyrillic:o})</sup>(\N{latin:C}|\N{cyrillic:Es})~\N{DEGREE SIGN}\N{latin:C}~gm;
}

sub _li_start {
  my( $self, $node, $rules ) = @_;

  my $bullet = '';
  $bullet = '*' if $node->parent->tag eq 'ul';
  $bullet = '-' if $node->parent->tag eq 'ol';

  return "\n$bullet ";
}

sub _link {
  my( $self, $node, $rules ) = @_;
  my $url = $node->attr('href') || '';
  my $text = $self->get_elem_contents($node) || '';
  
  if( my $title = $self->get_wiki_page($url) ) {
    # [[MiXed cAsE]] ==> <a href="http://site/wiki:mixed_case">MiXed cAsE</a>
    $title =~ s/_/ /g;
    return $text if $self->camel_case and lc $title eq lc $text and $self->is_camel_case($text);
    return "[[$text]]" if lc $text eq lc $title;
    return "[[$title|$text]]";
  } else {
    #return $url if $url eq $text; # Nice idea, but what will dokuwiki
    # do if the link is followed by punctuation in the text?
    # TODO: during postprocessing with regexes. 
    # (BTW, we'd better be aware that we
    # are not in a nowiki-text if we do this in postproceesing.)
    # If a rule conditionally depends on the surronding text, doing it in
    # the tree (here) is not straightforward at all; postprocessing of
    # the plain text is a better place for this.
    return "[[$url|$text]]";
  }
}

sub _image {
  my( $self, $node, $rules ) = @_;
  my $src = $node->attr('src') || '';
  return '' unless $src;

  my $w = $node->attr('width') || 0;
  my $h = $node->attr('height') || 0;
  if( $w and $h ) {
    $src .= "?${w}x${h}";
  } elsif( $w ) {
    $src .= "?${w}";
  }

  my $class = $node->attr('class') || '';
  my $padleft = $class eq 'mediaright' || $class eq 'mediacenter' ? ' ' : '';
  my $padright = $class eq 'medialeft' || $class eq 'mediacenter' ? ' ' : '';
  $src = "$padleft$src$padright";

  # All images considered external
  my $caption = $node->attr('title') || $node->attr('alt') || '';
  return "{{$src|$caption}}" if $caption;
  return "{{$src}}";
}

sub _td_start {
  my( $self, $node, $rules ) = @_;
  my $prefix = $node->tag eq 'th' ? '^' : '|';
  $prefix .= ' ';

  my $class = $node->attr('class') || '';
  $prefix .= ' ' if $class eq 'rightalign' or $class eq 'centeralign';

  return $prefix;
}

sub _td_end {
  my( $self, $node, $rules ) = @_;

  my $end = ' ';

  my $class = $node->attr('class') || '';
  $end .= ' ' if $class eq 'leftalign' or $class eq 'centeralign';

  my $colspan = $node->attr('colspan') || 1;

  my @right_cells = grep { $_->tag && $_->tag =~ /th|td/ } $node->right;
  return $end if $colspan == 1 and @right_cells;

  my $suffix = $node->tag eq 'th' ? '^' : '|';
  $suffix = ( $suffix ) x $colspan;
  return $end.$suffix;
}

sub preprocess_node {
  my( $self, $node ) = @_;
  if ( $node->tag eq 'a' ) {
      $self->strip_aname($node);
      return;
  } # strip_aname deletes the node, so all other actions you might
    # want to add here should be done unless this rule applied!
  # We simply return if we matched an A because no other rule applies.

  return if $self->strip_fonts_on_blank($node);
  # node deleted, so any other actions (you might want to add) would make no sense.

  $self->_mark_bodylevel_tables_boring($node) if $self->boring_tables_bodylevel;
  $self->_mark_complex_tables_boring($node);
}

sub __font_tags { 
  my( $self ) = @_;

  my @font_tags = qw/ font b i strong em /;
  # We leave S, U, TT, CODE, KEY tags untouched because they might be
  # used to produce visual effects even on blanks, or be semantically
  # interesting even on blanks. FONT is controversial.
  # Probably, we could leave it because the cases we might want to treat
  # should have been normalized into B and I etc. on a previous stage.

  # Well, we also need to strip U on blanks. (Attribute: strip_blank_underline)
  # (It's a hack useful for some messy files with links produced by MS Office:
  # the underlined blanks appear after the links.)
  if ( $self->strip_blank_underline ) {
      push @font_tags, 'u';
  }

  return @font_tags;
}

sub strip_fonts_on_blank {
  my( $self, $node ) = @_;
  my %font_tags = map { $_ => 1 } $self->__font_tags;

  return 0 
      unless $font_tags{$node->tag} and 
      not $node->look_down('_tag' => '~text', 'text' => qr /[^[:space:]]+/);
  $node->replace_with_content->delete();
  return 1;
}

# Mark a table's nodes "boring" if it is BODY's child.
# (When looking up, we jump over DIVs, because the MSOffice-generated
# docuements may include DIVs for sectioning between the TABLE and BODY).
sub _mark_bodylevel_tables_boring {
  my( $self, $node ) = @_;
  return unless $node->tag eq 'table';

  my $parent = $node->parent;
  $parent = $parent->look_up(sub {not $_[0]->tag eq 'div'}) if $parent;
  $self->_mark_this_table_boring($node)
      if not $parent or 
      $parent->tag eq 'body' or
      $parent->tag eq 'html';
}

# Mark a table's nodes "boring" if it is a "complex" table,
# i.e. includes nested tables.
sub _mark_complex_tables_boring {
  my( $self, $node ) = @_;
  $self->_mark_this_table_boring($node)
      if $node->tag eq 'table' and
      grep { $_->tag eq 'table' } $node->descendents;
  # TODO: descendents is not lazy! It would be more efficient to
  # implement this with a special procedure to look for matching
  # descendents. Unfortunately, HTML::Element::look_down doesn't suit
  # us: it includes the current node, too.
}

# Mark this table's nodes "boring".
sub _mark_this_table_boring {
  my( $self, $node ) = @_;
  $self->_transform_this_table($node, 
			       sub { my ($x) = @_; $x->tag('boring' . $x->tag); });
}

sub __table_node_tags { qw/ table tbody thead tfoot tr th td / }
sub __table_leaf_tags { qw/ th td / }

# Transform this table's nodes by applying a function.
# (Here, "this" table means the table we are inside.)
sub _transform_this_table {
  my( $self, $node, $transformation ) = @_;
  my %table_node_tags = map { $_ => 1 } $self->__table_node_tags;
  my %table_leaf_tags = map { $_ => 1 } $self->__table_leaf_tags;

  if ( $table_node_tags{$node->tag} ) {
      $transformation->($node);
  }

  return if ( $table_leaf_tags{$node->tag} );

  foreach my $child ( $node->content_list ) {
      $self->_transform_this_table($child, $transformation) unless $child->tag eq 'table';
  }
}

=head1 AUTHOR

David J. Iberri, C<< <diberri at cpan.org> >>

nested tables handling and some pretty formatting by Ivan Zakharyaschev, C<< <imz at altlinux.org> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bug-html-wikiconverter-dokuwiki at rt.cpan.org>, or through the web
interface at
L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=HTML-WikiConverter-DokuWiki>.
I will be notified, and then you'll automatically be notified of
progress on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc HTML::WikiConverter::DokuWiki

You can also look for information at:

=over 4

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/HTML-WikiConverter-DokuWiki>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/HTML-WikiConverter-DokuWiki>

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=HTML-WikiConverter-DokuWiki>

=item * Search CPAN

L<http://search.cpan.org/dist/HTML-WikiConverter-DokuWiki>

=back

=head1 COPYRIGHT & LICENSE

Copyright 2006 David J. Iberri, all rights reserved.
Copyright 2013 Ivan Zakharyaschev, all rights reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
